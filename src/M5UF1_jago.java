import java.util.Scanner;

public class M5UF1_jago{
    private Scanner sc;


    public M5UF1_jago() {
        this.sc = new Scanner(System.in);
    }

    public int readUserInput() {
        return sc.nextInt();
    }

    public void end() {
        System.out.println("FI");
    }


    void addArray_jago(){
        int[] array1 = new int[5];
        int[] array2 = new int[5];
        int[] array3 = new int[5];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = sc.nextInt();
        }
        for (int i = 0; i < array2.length; i++) {
            array2[i] = sc.nextInt();
        }
        for (int i = 0; i < array3.length; i++) {
            array3[i] = array1[i] + array2[i];
            System.out.print("[" + array3[i] + "]");
        }
    }

    void mulArray_jago(){
        int[] array1 = new int[5];
        int[] array2 = new int[5];
        int[] array3 = new int[5];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = sc.nextInt();
        }
        for (int i = 0; i < array2.length; i++) {
            array2[i] = sc.nextInt();
        }
        for (int i = 0; i < array3.length; i++) {
            array3[i] = array1[i] * array2[i];
            System.out.print("[" + array3[i] + "]");
        }
    }

    public void showMenu() {
        System.out.println("0.Salir del programa" );
        System.out.println("1.Sumar Arrays en paralelo");
        System.out.println("2.Multiplicar Arrays en paralelo");
        System.out.println("Escoge una opcion:");
    }

    public void operate(int opcio) {

        switch (opcio) {
            case 0:
                end();
                break;
            case 1:
                addArray_jago();
                break;
            case 2:
                mulArray_jago();
                break;
        }
    }
    public static void main(String[] args) {

        M5UF1_jago p5 = new M5UF1_jago();

        int response;
        do {
            p5.showMenu();
            response = p5.readUserInput();
            p5.operate(response);
        } while (response == 0);

    }


}
